#!/bin/bash

if [ $1 == "linear" ]
then
    ./hackerCipher newciphertext.txt quadgrams.txt words.txt $2

elif [ $1 == "mpi" ]
then
    mpirun -n $3 -machinefile hostsfile -ppn $3 ./hackerCipher newciphertext.txt quadgrams.txt words.txt $2
fi