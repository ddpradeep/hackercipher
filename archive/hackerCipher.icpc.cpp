#include <unordered_map>
#include "sys/time.h"
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <math.h>
#include "mkl.h"
using namespace std;

/***************************************************/
/*                Global Variables                 */
/***************************************************/
unordered_map<string, float> quadgram;
char    *cipher_text;
char    *cipher_text_clean;
char    *plain_text;
char    *cipher_key;
char    *freq_dist_key;
char    *ideal_cipher_key;
float   *freq_dist_value;
int     text_size;
int     clean_text_size;
int     qgram_size;

struct timeval time1;
float hacked_time=-1;

// Temporary scratchpad variables
char    *output_text;
char    *inverted_key;


/***************************************************/
/*               Function Prototypes               */
/***************************************************/
void load_quad_grams (char *filename);
void load_cipher_text (char *filename);
void inline resetTime(struct timeval *timer);
float inline getTime(struct timeval *time1);
void to_upper (char *input_string);
char *strip_nonalpha (char *input_string);
char *decrypt (char *cipher_text, int text_length, char *key);
float quality_of (char *text);
void frequency_analysis (char *text);
char *hacker_cipher(int nshots);
void swap_char (char *a, char *b);
int random (int min, int max);
void generate_ideal_cipher_key ();

/***************************************************/
/*                    main ()                      */
/***************************************************/
int main(int argc,  char** argv)
{
  if (argc !=4)
  {
    printf ("Invalid/Insufficient arguments passed.\n");
    printf ("Expected <cipher_text_file> <quadgram_file> <no_of_shots>\n");
    return -1;
  }
  /* Start Profiling */
  resetTime(&time1);

  // 1. Read cipher text input
  load_cipher_text (argv[1]);
  to_upper (cipher_text);

  // 2. Load Quadgrams
  load_quad_grams (argv[2]);

  // 3. Strip nonalphabetical characters
  cipher_text_clean = strip_nonalpha (cipher_text);
  clean_text_size   = strlen (cipher_text_clean) + 1;

  // 4. Get frequency distribution of cipher text
  frequency_analysis(cipher_text_clean);

  // 5. Generate ideal cipher key to start hacking with
  generate_ideal_cipher_key();

  // 6. Call the core algorithm to hack the cipher text
  hacker_cipher (atoi(argv[3]));

  /* Stop Profiling */
  printf ("\nRun time: %.2f s\n\n", getTime(&time1));

  // 7. Print the results
  if (hacked_time>0)  // Check if any key has been found using time taken to hack (-1 by default)
  {
    printf ("***********************************************\n");
    printf ("*                   RESULTS                   *\n");
    printf ("***********************************************\n");
    printf ("\nBest Key: %s\n", cipher_key);
    printf ("\nFound in: %.2fs\n", hacked_time);
    printf ("\nDeciphered Text:\n\n%s\n\n", plain_text);
    printf ("***********************************************\n");
  }
  
  return 0;
}


/***************************************************/
/* FILE I/O: Reads and loads the Cipher text       */
/***************************************************/
void load_cipher_text (char *filename)
{
  // 1. Get text size
  ifstream cipher_text_file(filename, ios::binary | ios::ate);
  text_size = cipher_text_file.tellg();
  cipher_text_file.close();

  // 2. Allocate space for cipher_text, plain_text and cipher_key
  cipher_text         = (char*) MKL_malloc(sizeof(char)*text_size,32);
  plain_text          = (char*) MKL_malloc(sizeof(char)*text_size,32);
  cipher_key          = (char*) MKL_malloc(sizeof(char)*27,32);
  strcpy (cipher_text, "");

  // 3. Open file
  cipher_text_file.open(filename);

  // 4. Initialize temporary placeholders for parsing data
  string line;
  if (cipher_text_file.is_open())
  {
    // 5. Read a line from file
    while(getline(cipher_text_file, line))
    {

      // 6. Load the data into a virtual "dictionary" vector
      strcat(cipher_text, line.c_str());
    }
    printf ("%d characters read from %s\n", text_size, filename);
  }

  // 8. Update text length
  text_size = strlen (cipher_text);

  // 7. Close file
  cipher_text_file.close();
}


/***************************************************/
/* FILE I/O: Reads and loads Quadgrams             */
/***************************************************/
void load_quad_grams (char *filename)
{
  // 1. Open file
  ifstream qgram_file(filename);

  // 2. Initialize temporary placeholders for parsing data
  string line;
  int qgram_count;
  char qgram_name[5];
  qgram_size = 0;
  float sum_counts = 0;
  if (qgram_file.is_open())
  {
    // 3. Read a line from file
    while(getline(qgram_file, line))
    {
      // 4. Separate the read line into qgram and count
      stringstream sl(line);
      sl >> qgram_name >> qgram_count;

      // 5. Load the data into dictionary
      quadgram [qgram_name] = qgram_count;

      // 6. Accumulate count values read
      sum_counts += qgram_count;
      qgram_size ++;
    }

    printf ("%d quadgrams loaded from %s\n", qgram_size, filename);
  }

  // 7. Close file
  qgram_file.close();

  // 8. Convert counts to logarithmic probabilities
  for ( auto qgram = quadgram.begin(); qgram!= quadgram.end(); ++qgram )
    qgram->second = log10 (qgram->second / sum_counts);
}


/*******************************************************/
/* ANALYSIS: Perform frequency analysis of cipher text */
/*******************************************************/
void frequency_analysis (char *text)
{
  // 1. Allocate "dictionary" variables
  freq_dist_key   = (char*)   MKL_malloc(sizeof(char)*27,32);
  freq_dist_value = (float*)  MKL_malloc(sizeof(float)*26,32);

  // 2. Build key
  strcpy (freq_dist_key, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  // 3. Build value
  for (int i=0; i<26; i++)
  {
    unsigned int count = 0;
    for (int j=0; j<clean_text_size; j++)
      if (text[j]==('A'+i))
        count ++;
    freq_dist_value[i] = (float)count*100.0/(float)clean_text_size;
  }

  // 4. Sort the frequency distribution
  #pragma novector
  for (int i = 0; i<26; i++)
    for (int j = 0; j<25; j++)
      if (freq_dist_value[j]<freq_dist_value[j+1])
      {
        // Swap key
        char tempc = freq_dist_key[j+1];
        freq_dist_key[j+1]  = freq_dist_key[j];
        freq_dist_key[j]    = tempc;

        // Swap value
        float tempf = freq_dist_value[j+1];
        freq_dist_value[j+1]  = freq_dist_value[j];
        freq_dist_value[j]    = tempf;
      }
}


/***************************************************************************************/
/* ANALYSIS: Generates an ideal cipher text based on letter frequencies in cipher text */
/***************************************************************************************/
void generate_ideal_cipher_key ()
{
  // 1. Sequence of english alphabets based on decreasing frequency of usage (from Wikipedia)
  char letter_dist [] = "ETAOINSHRDLCUMWFGYPBVKJXQZ";

  // 2. Allocate variable
  ideal_cipher_key = (char*)   MKL_malloc(sizeof(char)*27,32);

  // 3. Match sorted frequency distribution to universal letter distribution
  for (int i=0; i<26; i++)
  {
    unsigned int count = 0;
    for (int j=0; j<26; j++)
      if (letter_dist[j]==('A'+i))
      {
        ideal_cipher_key [i] = freq_dist_key[j];
        break;
      }
  }
  ideal_cipher_key [26] = '\0';

  printf ("Starting attack using ideally generated cipher key (%s)\n\n", ideal_cipher_key);
}


/*******************************************************/
/* ANALYSIS: Measure the englishness of the plain text */
/*******************************************************/
float quality_of (char *text)
{
  // 1. Initialize local variables
  int text_size = strlen (text);
  float quality = 0;
  char qgram [5];

  // 2. Start measuring quality
  #pragma novector
  for (int i=0; i<text_size-4; i++)
  {
    // 3. For every quad gram extracted from the input text,
    strncpy (qgram, text+i, 4);

    // 5. Accumulate probability information from the quadgram statistics
    unordered_map<string,float>::const_iterator match = quadgram.find (qgram);
    if ( match != quadgram.end() )
      quality += match->second;
    else
      quality += -10.0;             // If not found, use a prefixed quality
  }

  // 6. Return accumulated quality measure
  return quality;
}


/*********************************************************/
/* CORE: Hacks the cipher text to find the closest match */
/*********************************************************/
char *hacker_cipher (int nshots)
{
  // 1. Allocate and initialize local variables
  float best_quality, shot_quality, subshot_quality;

  char    *shot_cipher_key    = (char*) MKL_malloc(sizeof(char)*27,32);
  char    *subshot_cipher_key = (char*) MKL_malloc(sizeof(char)*27,32);
  char    *temp_plain_text;

  output_text                 = (char*) MKL_malloc(sizeof(char)*text_size,32);
  inverted_key                = (char*) MKL_malloc(sizeof(char)*27,32);

  // 2. Set current quality state to minimum
  best_quality = -10000000000.0;

  for (int shot=1; shot<=nshots; shot++)
  {
    printf ("Executing shot %d\t", shot);

    // 3. Assign a ideal key map
    strcpy(shot_cipher_key, ideal_cipher_key);
    
    // 4. Get a baseline for quality
    temp_plain_text = decrypt (cipher_text_clean, clean_text_size, shot_cipher_key);
    shot_quality = quality_of(temp_plain_text);

    // 5. Get a better key by Luck with Brute-Force (LBF)
    for (int subshot=0; subshot<1000; subshot++)
    {
      // i. Try a different variation of the base key
      strcpy (subshot_cipher_key, shot_cipher_key);
      swap_char(subshot_cipher_key+random(0, 25), subshot_cipher_key+random(0, 25));
      // printf ("%d)KEY: %s\n", subshot, subshot_cipher_key);

      // ii. Get the quality measure
      temp_plain_text = decrypt (cipher_text_clean, clean_text_size, subshot_cipher_key);
      subshot_quality = quality_of(temp_plain_text);

      // iii. If improvement is seen, repeat another 1000 times
      if (subshot_quality>shot_quality)
      {
        shot_quality = subshot_quality;
        strcpy (shot_cipher_key, subshot_cipher_key);
        subshot = 0;
      }
    }

    // 6. If the best key results in a higher quality than previous shots, update baseline
    if (shot_quality>best_quality && strcmp (cipher_key, shot_cipher_key))
    {
      best_quality = shot_quality;
      strcpy (cipher_key, shot_cipher_key);
      hacked_time = getTime(&time1);
      printf (": <Better Cipher Key Found>\tTime: %.2fs\n\n", hacked_time);
      // printf ("REF: %s\n", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
      printf ("\tKEY - %s\n\n", cipher_key);
      // printf ("QLT: %.2f\n", best_quality);
      temp_plain_text = decrypt (cipher_text_clean, clean_text_size, cipher_key);
      // printf ("\tDeciphered Text\t: %s\n\n", temp_plain_text+(strlen(temp_plain_text)-8));
    }
    else
      printf (": <No Better Key>\t\tTime: %.2fs\n", getTime(&time1));
  }

  // 7. Generate the plain text with the best cipher key
  temp_plain_text = decrypt (cipher_text, text_size, cipher_key);
  strcpy (plain_text, temp_plain_text);

  // 8. Wrapup and return the best cipher key
  MKL_free(shot_cipher_key);
  MKL_free(subshot_cipher_key);
  MKL_free(output_text);
  MKL_free(inverted_key);
  return cipher_key;
}


/********************************************************************/
/* DECRYPTION: Decrypts the cipher text with the passed cipher key  */
/********************************************************************/
char *decrypt (char *cipher_text, int text_length, char *key)
{
  // 1. Allocate variables

  inverted_key [26] = '\0';

  // 2. Build inverted key
  #pragma novector
  for (int i=0; i<26; i++)
  {
    // Find the letter to map to
    char toChar=0;
    #pragma novector
    for (int j=0; j<26; j++)
      if ((key[j])==i+'A')
      {
        toChar = j+'A';
      }

    // Map it to inverted key
    inverted_key [i] = toChar;
  }

  // 3. Perform monoalphabatic substitution
  #pragma novector
  for (int i=0; i<text_length; i++)
  {
    if (cipher_text[i]>='A' && cipher_text[i]<='Z')
      output_text[i] = inverted_key[cipher_text[i]-'A'];
    else
      output_text[i] = cipher_text[i];
  }
  output_text [text_length] = '\0';

  // 4. Return array address
  return output_text;
}

/***************************************************/
/*        Converts lower case to upper case        */
/***************************************************/
void to_upper (char *input_string)
{
  for (int i=0; i<text_size; i++)
  {
      if (input_string[i]>='a' && input_string[i]<='z')
        input_string[i] -= 32;
  }

}


/***************************************************/
/*       Removes Non-alphabetical Characters       */
/***************************************************/
char *strip_nonalpha (char *input_string)
{
  // 1. Count the resultant string length
  int length = 0;
  #pragma novector
  for (int i=0; i<text_size; i++)
  {
      if ((input_string[i]>='A' && input_string[i]<='Z'))
      {
        length ++;
      }
  }

  // 2. Allocate space for new string
  char *output_string = (char*) MKL_malloc(sizeof(char)*(length+1),32);

  // 3. Strip unwanted characters
  int ptr=0;
  #pragma novector
  for (int i=0; i<text_size; i++)
  {
      if ((input_string[i]>='A' && input_string[i]<='Z'))
      {
        output_string [ptr++] = input_string [i];
      }
  }
  output_string [ptr] = '\0';

  // 4. Return address of new string
  return output_string;
}

/***************************************************/
/*             Random Number Generator             */
/***************************************************/
int random (int min, int max)
{
  return (min + (rand() % (int)(max - min + 1)));
}

/***************************************************/
/*               Swaps Two Characters              */
/***************************************************/
void swap_char (char *a, char *b)
{
  char temp = *a;
  *a = *b;
  *b = temp;
}


/***************************************************/
/*                Profiling Functions              */
/***************************************************/
void resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}