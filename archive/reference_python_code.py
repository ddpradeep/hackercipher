__author__ = 'Pradeep'
from collections import OrderedDict
from datetime import datetime
# import argparse
import operator
import random
import math
import sys
import os
import re

LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
QUADGRAM = None
START_TIME = None
END_TIME = None


############################################
# main () function: Program starts here
############################################
def main():

    # 1. Handle command line inputs
    # handle_command_line_inputs();

    # 2. Load the input ciphertext file
    # cipher_text = load_text_file('newciphertext.txt')
    cipher_text = load_text_file('newciphertext.txt')

    # 3. Get the best match plain text
    global START_TIME, END_TIME
    START_TIME = datetime.now()
    cipher_key = cipher_text_hacker(cipher_text, 5)


############################################
# Handler for input args
############################################
# def handle_command_line_inputs ():
#     parser = argparse.ArgumentParser(description='Description of your program')
#     parser.add_argument('-f', '--foo', help='Description for foo argument', required=False)
#     parser.add_argument('-b', '--bar', help='Description for bar argument', required=False)
#     args = vars(parser.parse_args())


############################################
# Returns the contents of a file
############################################
def load_text_file(filename):
    # If the input file does not exist, exit
    if not os.path.exists(filename):
        print('The file %s does not exist. Quitting...' % (filename))
        sys.exit()
    # Else, parse the file contents
    else:
        # 1. Open the input file
        cipher_text_file = open(filename)

        # 2. Read as a whole block
        content = cipher_text_file.read()

        # 3. Wrapup and return the contents
        cipher_text_file.close()
        print "Read CipherText (%s) of length: %d Bytes" % (filename, len(content))
        return content


############################################
# Decipher a ciphertext using supplied key
############################################
def sub_decipher(cipher_text, cipher_key):
    # 1. Inverse the key, so that key[0-25] reflects the replacement for A-Z
    inverse_key = [LETTERS[(cipher_key.index(letter)) % 26] for letter in LETTERS]
    # 2. Build the plain text
    plain_text = ''
    for c in cipher_text:
        if c.isalpha():
            # Decipher the text
            plain_text += inverse_key[ord(c.upper())-ord('A')]
        else:
            # Retain puctuations and numbers
            plain_text += c

    # 3. Return the plain text
    return plain_text


#####################################################
# CORE: Performs Monoalphabatic Substitution Hacking
#####################################################
def cipher_text_hacker(cipher_text, nshots):
    # 1. Read Quadgrams
    global QUADGRAM
    QUADGRAM = load_quadgrams('quadgrams.txt')
    shot = 1
    best_quality = -1.0*sys.float_info.max
    best_cipher_key = None

    # 1. Remove all non textual characters
    cipher_text_clean = re.sub('[^A-Z]', '', cipher_text.upper())
    print "Striped length:", len(cipher_text_clean);
    
    # Get the frequency distribution for cipher_text
    print "\nCIPHER TEXT:"
    cipher_text_freq = {}
    for letter in LETTERS:
        cipher_text_freq[letter] = cipher_text_clean.count(letter)*100/float(len(cipher_text_clean))
    cipher_text_freq = OrderedDict(sorted(cipher_text_freq.iteritems(), key=operator.itemgetter(1), reverse=True))
    for key in cipher_text_freq:
        print "   %s  " % key,
    print ""
    for key in cipher_text_freq:
        print " %2.2f " % (cipher_text_freq[key]),
    print ""

    while shot <= nshots:
        # Update iteration counter
        print "Executing HackShot %d" % shot

        # 2. Assign default cipher key (1st try)
        shot_cipher_key = list(LETTERS)

        # 3. Shuffle it (Improve this logic - Chap 18)
        random.shuffle(shot_cipher_key)
        
        # 4. Try to decipher using the key and measure plain text quality
        shot_quality = get_quality(decrypt(cipher_text_clean, shot_cipher_key))

        # 5. Try to generate better plain texts atleasts 1000 times
        subshot = 0
        while subshot < 1000:
            # Swap any two mappings randomly
            a = random.randint(0, 25)
            b = random.randint(0, 25)
            subshot_cipher_key = shot_cipher_key[:]
            subshot_cipher_key[a], subshot_cipher_key[b] = subshot_cipher_key[b], subshot_cipher_key[a]

            # Measure plain text quality
            subshot_quality = get_quality(decrypt(cipher_text_clean, subshot_cipher_key))
            subshot += 1

            # If it worked out well, repeat it 1000 times
            if subshot_quality > shot_quality:
                shot_quality, shot_cipher_key = subshot_quality, subshot_cipher_key[:]
                subshot = 0

        if shot_quality > best_quality:
            best_quality = shot_quality
            best_cipher_key = ''.join(shot_cipher_key[:])
            plain_text = sub_decipher(cipher_text, best_cipher_key)
            print "Shot quality:", shot_quality
            print_results(plain_text, cipher_text, best_cipher_key)

        shot += 1

    return best_cipher_key


############################################
# Decipher a ciphertext using supplied key
############################################
def decrypt(cipher_text, cipher_key):
    # 1. Inverse the key, so that key[0-25] reflects the replacement for A-Z
    inverse_key = [LETTERS[(cipher_key.index(letter)) % 26] for letter in LETTERS]
    
    # 2. Build the plain text
    plain_text = ''
    for c in cipher_text:
        if c.isalpha():
            # Decipher the text
            plain_text += inverse_key[ord(c.upper())-ord('A')]
        else:
            # Retain puctuations and numbers
            plain_text += c

    # 3. Return the plain text
    return plain_text


#################################################
# Parses quadgram data from file into dictionary
#################################################
def load_quadgrams(filename):
        """ load a file containing ngrams and counts, calculate log probabilities """
        quadgrams = {}
        # 1. Parse ngrams from file into variables
        for line in file(filename):
            # Read every line from file
            qgram, count = line.split(' ')
            # Store ngram & count in dict
            quadgrams[qgram] = int(count)

        # 2. Update ngram_type (2,3 or 4) and sum of all counts
        sum_quadgram_counts = sum(quadgrams.itervalues())

        # 3. Calculate log probablilities for every ngram
        for qgram in quadgrams.keys():
            # log (count/total_count)
            quadgrams[qgram] = math.log10(float(quadgrams[qgram])/sum_quadgram_counts)
            # print "%s\t%f" % (qgram, quadgrams[qgram])

        return quadgrams


###################################################################
# Measures the quality of the plain text using quadgram statistics
###################################################################
def get_quality(plain_text):
    global QUADGRAM
    """ compute the score of text """
    quality = 0
    for i in xrange(len(plain_text)-4+1):
        if plain_text[i:i+4] in QUADGRAM:
            quality += QUADGRAM[plain_text[i:i+4]]
        else:
            quality += -11.625
    return quality


#####################################################
# Print Useful Results
#####################################################
def print_results(plain_text, cipher_text, cipher_key):

    # 1. Print Cipher Key and Decoded Text
    print "\nCIPHER KEY:", cipher_key
    print "PLAIN TEXT:", plain_text[-10:-2]

    # 2. Print Execution Time
    print "Execution Time:", datetime.now() - START_TIME

    # print "ALPHABETICAL:"
    # for letter in LETTERS:
    #     print "   %s  " % letter,
    # print ""
    # for letter in LETTERS:
    #     print " %2.2f " % (cur_freq[letter]),
    # print ""

    # 3. Print frequency distribution of cipher text
    return
    print "\nCIPHER TEXT:"
    # Remove punctuations
    cipher_text = re.sub('[^A-Z]', '', cipher_text.upper())
    cipher_text_freq = {}
    # Build frequency dictionary mapping (alphabetically)
    for letter in LETTERS:
        cipher_text_freq[letter] = cipher_text.count(letter)*100/float(len(cipher_text))
    # Sort the frequency distribution by frequency
    cipher_text_freq = OrderedDict(sorted(cipher_text_freq.iteritems(), key=operator.itemgetter(1), reverse=True))
    # Display the distribution
    for key in cipher_text_freq:
        print "   %s  " % key,
    print ""
    for key in cipher_text_freq:
        print " %2.2f " % (cipher_text_freq[key]),
    print ""

    # 4. Print frequency distribution of plain text
    print "\nPLAIN TEXT:"
    # Remove punctuations
    plain_text = re.sub('[^A-Z]', '', plain_text.upper())
    plain_text_freq = {}
    # Build frequency dictionary mapping (alphabetically)
    for letter in LETTERS:
        plain_text_freq[letter] = plain_text.count(letter)*100/float(len(plain_text))
    # Sort the frequency distribution by frequency
    plain_text_freq = OrderedDict(sorted(plain_text_freq.iteritems(), key=operator.itemgetter(1), reverse=True))
    # Display the distribution
    for key in plain_text_freq:
        print "   %s  " % key,
    print ""
    for key in plain_text_freq:
        print " %2.2f " % (plain_text_freq[key]),
    print ""

if __name__ == '__main__':
    main()



