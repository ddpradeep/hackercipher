CC=g++
MPICC=mpicc
FFLAGS=-Ofast -std=c++11

linear: hackerCipher.gcc.cpp
	$(CC) $(FFLAGS) hackerCipher.gcc.cpp -o hackerCipher

mpi: hackerCipher.mpi.cpp
	$(MPICC) $(FFLAGS) hackerCipher.mpi.cpp -o hackerCipher -lstdc++

clean:
	rm -f hackerCipher
