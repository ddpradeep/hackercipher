This package contains sequential and parallel implementations of the same program, which used to decipher monoalphabetic ciphers using frequency analysis and 'hill climbing' algorithem. 

The program decodes the input spaceless ciphertext into spaceless English plaintext. (Optional) Add-on feature splits the spaceless plaintext into words, for incread readability.

#  Sequential
1. Sequential code is intended to run using a single core.
2. Input:
	- ciphertext file <newciphertext.txt>
	- quadgram reference file name <quadgrams.txt>
	- dictionary list file name <words.txt>
	- number of shots to run (maximum number of attempts) <*int*>
3. Compiler required: 
	- GNU's gcc version 4.7.3 or higher
4. To compile: 

      `g++ -Ofast -std=c++11 hackerCipher.gcc.cpp -o hackerCipher`
       
	or `make linear`

5. To execute

	 `./hackerCipher newciphertext.txt quadgrams.txt words.txt $2`
	 
	or `./run.sh linear`
	
#  Parallel
1. Parallel code is intended to run using multi-core computer.
2. Input:
	- ciphertext file <newciphertext.txt>
	- quadgram reference file name <quadgrams.txt>
	- dictionary list file name <words.txt>
	- number of shots to run (maximum number of attempts) <*int*>
	- number of processors to use (np & ppn) <*int*>
3. Compiler required: 
	- GNU's gcc version 4.7.3 or higher
4. Dependencies:
	- MPI library, e.g. OpenMP, MPICH or Intel MPI (tested on Linux Centos 6.5 with Intel MPI v4.1)
4. To compile: 

      `mpicc -Ofast hackerCipher.mpi.cpp -o hackerCipher -lstdc++`
       
	or `make mpi`

5. To execute

	 `mpirun -n $3 -machinefile hostsfile -ppn $3 ./hackerCipher newciphertext.txt quadgrams.txt words.txt $2`
	 
	or `./run.sh mpi`
	
