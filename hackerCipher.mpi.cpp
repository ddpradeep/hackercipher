/***********************************************************************************/
/*                      HackerCipher v1.1 (Parallel)                               */
/***********************************************************************************/
/*              * This program tries to hack the monoalphabetic cipher             */ 
/* Description  * using Frequency Analysis and Hill Climbing algorithm             */
/*              * using a single core.                                             */
/***********************************************************************************/
/*              * 1. cipher text - file name         (char[])                      */
/*  Input Args  * 2. quadgram reference - file name  (char[])                      */
/*              * 3. dictionary list - file name     (char[])                      */
/*              * 4. number of shots to run          (int)                         */
/*              * >> Number of processes (np & ppn) to be passed to mpirun         */
/***********************************************************************************/
/* Requirements * gcc version 4.7.3 or greater                                     */
/*              * mpi library (tested on Linux with Intel MPI Library) v4.1        */
/***********************************************************************************/
/*  To Compile  * mpicc -Ofast -std=c++11 <this_file.cpp> -o <output_name> -lstdc++*/
/*              * (Note: Edit HUMAN_READABLE flag (line 24) first)                 */
/***********************************************************************************/
/*   Authors    * Pradeep, Han Jianglei, Ng Chun Hong                              */
/***********************************************************************************/


#define HUMAN_READABLE true       // [true/false] Enables or disables post processing of plain text to separate the into words
                                  // Default is false

#include "mpi.h"
#include <unordered_map>
#include "sys/time.h"
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <math.h>
using namespace std;

/***************************************************/
/*                Global Variables                 */
/***************************************************/
unordered_map<string, float> quadgram;
char    *cipher_text;
char    *cipher_text_clean;
char    *plain_text;
char    *readable_text;
char    *cipher_key;
char    *freq_dist_key;
char    *ideal_cipher_key;
float   *freq_dist_value;
int     text_size;
int     clean_text_size;
int     qgram_size;
int   *text_map;
char  **dictionary;
int   word_list_size;

clock_t time1;
float hacked_time=-1;

// Temporary scratchpad variables
char    *output_text;
char    *inverted_key;

// MPI control variables
MPI_Status status;
MPI_Group comm_group, group;
MPI_Win win;
int mpi_total_proc;
int mpi_rank;

#define MASTER 0


/***************************************************/
/*               Function Prototypes               */
/***************************************************/
void load_quad_grams (char *filename);
void load_cipher_text (char *filename);
void load_dictionary (char *filename);
void inline resetTime(clock_t *timer);
float inline getTime(clock_t *time1);
void to_upper (char *input_string);
char *strip_nonalpha (char *input_string);
char *decrypt (char *cipher_text, int text_length, char *key);
float quality_of (char *text);
void frequency_analysis (char *text);
char *hacker_cipher(int nshots);
void swap_char (char *a, char *b);
int random (int min, int max);
void generate_ideal_cipher_key ();
void word_separator();


/***************************************************/
/*                    main ()                      */
/***************************************************/
int main(int argc,  char** argv)
{
  /***********MPI INIT********/
  MPI_Init (&argc, &argv);                            // Initialization of MPI environment
  MPI_Comm_size (MPI_COMM_WORLD, &mpi_total_proc);    // Get total processes count
  MPI_Comm_rank (MPI_COMM_WORLD,&mpi_rank);           // Get current process rank
  /***************************/

  if (argc !=5)
  {
    if (mpi_rank==MASTER)
    {
      printf ("Invalid/Insufficient arguments passed.\n");
    printf ("Expected <cipher_text_file> <quadgram_file> <dictionary_file> <no_of_shots>\n");
    }
    MPI_Finalize();
    return -1;
  }

  /* Start Profiling */
  resetTime(&time1);

  // 1. Read cipher text input
  load_cipher_text (argv[1]);
  to_upper (cipher_text);

  // 2. Load Quadgrams
  load_quad_grams (argv[2]);

  // 3. Strip nonalphabetical characters
  cipher_text_clean = strip_nonalpha (cipher_text);
  clean_text_size   = strlen (cipher_text_clean) + 1;

  // 4. Get frequency distribution of cipher text
  frequency_analysis(cipher_text_clean);

  // 5. Generate ideal cipher key to start hacking with
  generate_ideal_cipher_key();

  /**********MPI SYNC*********/
  MPI_Barrier (MPI_COMM_WORLD);
  /***************************/

  // 6. Call the core algorithm to hack the cipher text
  hacker_cipher (atoi(argv[4]));

  // 7. Convert the nonsensical text readable format (if requested)
  if (mpi_rank == MASTER && HUMAN_READABLE)
  {
    load_dictionary (argv[3]);
    word_separator ();
    strcpy (plain_text, readable_text);
  }

  /* Stop Profiling */
  if (mpi_rank==MASTER)
    printf ("\nRun time: %.2f s\n\n", getTime(&time1));

  // 7. Print the results
  if (hacked_time>0 && mpi_rank==MASTER)  // Check if any key has been found using time taken to hack (-1 by default)
  {
    printf ("***********************************************\n");
    printf ("*                   RESULTS                   *\n");
    printf ("***********************************************\n");
    printf ("\nBest Key: %s\n", cipher_key);
    printf ("\nDeciphered Text:\n\n%s\n\n", plain_text);
    printf ("***********************************************\n");
  }
  
  /*********MPI FINISH********/
  MPI_Finalize ();
  /***************************/

  return 0;
}


/***************************************************/
/* FILE I/O: Reads and loads the Cipher text       */
/***************************************************/
void load_cipher_text (char *filename)
{
  // 1. Get text size
  ifstream cipher_text_file(filename, ios::binary | ios::ate);
  text_size = cipher_text_file.tellg();
  cipher_text_file.close();

  // 2. Allocate space for cipher_text, plain_text and cipher_key
  cipher_text         = new char [text_size];
  plain_text          = new char [text_size*2];
  cipher_key          = new char [27];
  strcpy (cipher_text, "");

  // 3. Open file
  cipher_text_file.open(filename);

  // 4. Initialize temporary placeholders for parsing data
  string line;
  if (cipher_text_file.is_open())
  {
    // 5. Read a line from file
    while(getline(cipher_text_file, line))
    {

      // 6. Load the data into a virtual "dictionary" vector
      strcat(cipher_text, line.c_str());
    }
    if (mpi_rank==MASTER)
      printf ("%d characters read from %s\n", text_size, filename);
  }

  // 8. Update text length
  text_size = strlen (cipher_text);

  // 7. Close file
  cipher_text_file.close();
}


/***************************************************/
/* FILE I/O: Reads and loads Quadgrams             */
/***************************************************/
void load_quad_grams (char *filename)
{
  // 1. Open file
  ifstream qgram_file(filename);

  // 2. Initialize temporary placeholders for parsing data
  string line;
  int qgram_count;
  char qgram_name[5];
  qgram_size = 0;
  float sum_counts = 0;
  if (qgram_file.is_open())
  {
    // 3. Read a line from file
    while(getline(qgram_file, line))
    {
      // 4. Separate the read line into qgram and count
      stringstream sl(line);
      sl >> qgram_name >> qgram_count;

      // 5. Load the data into dictionary
      quadgram [qgram_name] = qgram_count;

      // 6. Accumulate count values read
      sum_counts += qgram_count;
      qgram_size ++;
    }
    if (mpi_rank==MASTER)
      printf ("%d quadgrams loaded from %s\n", qgram_size, filename);
  }

  // 7. Close file
  qgram_file.close();

  // 8. Convert counts to logarithmic probabilities
  for ( auto qgram = quadgram.begin(); qgram!= quadgram.end(); ++qgram )
    qgram->second = log10 (qgram->second / sum_counts);
}


/***************************************************/
/* FILE I/O: Reads and loads word list             */
/***************************************************/
void load_dictionary (char *filename)
{
  // 1. Get text size
  ifstream dictionary_file(filename, ios::binary | ios::ate);
  text_size = dictionary_file.tellg();
  dictionary_file.close();

  // 2. Allocate space for cipher_text, plain_text and cipher_key
  dictionary = new char*[text_size];
  for(int i = 0; i < text_size; ++i)
    dictionary[i] = new char[30];

  // 3. Open file
  dictionary_file.open(filename);

  // 4. Initialize temporary placeholders for parsing data
  string line;
  int word = 0;

  if (dictionary_file.is_open())
  {
    // 5. Read a line from file
    while(getline(dictionary_file, line))
    {
      // 6. Load the data into a virtual "dictionary" vector
      strcpy(dictionary[word++], line.c_str());
    }

    printf ("%d words read from %s\n", word, filename);
  }

  // 8. Update text length
  word_list_size = word;

  // 7. Close file
  dictionary_file.close();
}


/*******************************************************/
/* ANALYSIS: Perform frequency analysis of cipher text */
/*******************************************************/
void frequency_analysis (char *text)
{
  // 1. Allocate "dictionary" variables
  freq_dist_key   = new char [27];
  freq_dist_value = new float [26];

  // 2. Build key
  strcpy (freq_dist_key, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  // 3. Build value
  for (int i=0; i<26; i++)
  {
    unsigned int count = 0;
    for (int j=0; j<clean_text_size; j++)
      if (text[j]==('A'+i))
        count ++;
    freq_dist_value[i] = (float)count*100.0/(float)clean_text_size;
  }

  // 4. Sort the frequency distribution
  #pragma novector
  for (int i = 0; i<26; i++)
    for (int j = 0; j<25; j++)
      if (freq_dist_value[j]<freq_dist_value[j+1])
      {
        // Swap key
        char tempc = freq_dist_key[j+1];
        freq_dist_key[j+1]  = freq_dist_key[j];
        freq_dist_key[j]    = tempc;

        // Swap value
        float tempf = freq_dist_value[j+1];
        freq_dist_value[j+1]  = freq_dist_value[j];
        freq_dist_value[j]    = tempf;
      }
}


/***************************************************************************************/
/* ANALYSIS: Generates an ideal cipher text based on letter frequencies in cipher text */
/***************************************************************************************/
void generate_ideal_cipher_key ()
{
  // 1. Sequence of english alphabets based on decreasing frequency of usage (from Wikipedia)
  char letter_dist [] = "ETAOINSHRDLCUMWFGYPBVKJXQZ";

  // 2. Allocate variable
  ideal_cipher_key = new char [27];

  // 3. Match sorted frequency distribution to universal letter distribution
  for (int i=0; i<26; i++)
  {
    unsigned int count = 0;
    for (int j=0; j<26; j++)
      if (letter_dist[j]==('A'+i))
      {
        ideal_cipher_key [i] = freq_dist_key[j];
        break;
      }
  }
  ideal_cipher_key [26] = '\0';
  if (mpi_rank==MASTER)
    printf ("Starting attack using ideally generated cipher key (%s)\n\n", ideal_cipher_key);
}


/*******************************************************/
/* ANALYSIS: Measure the englishness of the plain text */
/*******************************************************/
float quality_of (char *text)
{
  // 1. Initialize local variables
  int text_size = strlen (text);
  float quality = 0;
  char qgram [5];

  // 2. Start measuring quality
  #pragma novector
  for (int i=0; i<text_size-4; i++)
  {
    // 3. For every quad gram extracted from the input text,
    strncpy (qgram, text+i, 4);

    // 5. Accumulate probability information from the quadgram statistics
    unordered_map<string,float>::const_iterator match = quadgram.find (qgram);
    if ( match != quadgram.end() )
      quality += match->second;
    else
      quality += -10.0;             // If not found, use a prefixed quality
  }

  // 6. Return accumulated quality measure
  return quality;
}


/*********************************************************/
/* CORE: Hacks the cipher text to find the closest match */
/*********************************************************/
char *hacker_cipher (int nshots)
{
  // 1. Allocate and initialize local variables
  float best_quality, shot_quality, subshot_quality, last_best_found_time;
  char    *shot_cipher_key    = new char [27];
  char    *subshot_cipher_key = new char [27];
  char    *temp_plain_text;

  output_text                 = new char [text_size];
  inverted_key                = new char [27];

  char    *mpi_buf_cipher_key = new char [27];
  float    mpi_buf_quality, mpi_buf_hacked_time;

  // 2. Set current quality state to minimum
  best_quality = -10000000000.0;

  for (int shot=1; shot<=nshots; shot++)
  {
    MPI_Barrier (MPI_COMM_WORLD);

    if (mpi_rank==MASTER)
      printf ("Executing shot %d\t", shot);

    // 3. Assign a ideal key map
    strcpy(shot_cipher_key, ideal_cipher_key);
    
    // 4. Get a baseline for quality
    temp_plain_text = decrypt (cipher_text_clean, clean_text_size, shot_cipher_key);
    shot_quality = quality_of(temp_plain_text);

    // 5. Get a better key by Luck with Brute-Force (LBF)
    for (int subshot=0; subshot<1000; subshot++)
    {
      // i. Try a different variation of the base key
      strcpy (subshot_cipher_key, shot_cipher_key);
      swap_char(subshot_cipher_key+random(0, 25), subshot_cipher_key+random(0, 25));
      // printf ("%d)KEY: %s\n", subshot, subshot_cipher_key);

      // ii. Get the quality measure
      temp_plain_text = decrypt (cipher_text_clean, clean_text_size, subshot_cipher_key);
      subshot_quality = quality_of(temp_plain_text);

      // iii. If improvement is seen, log the time it found and repeat another 1000 times
      if (subshot_quality>shot_quality)
      {
        shot_quality = subshot_quality;
        last_best_found_time = getTime(&time1);
        strcpy (shot_cipher_key, subshot_cipher_key);

        subshot = 0;
      }
    }

    // 6. If the best key results in a higher quality than previous shots, update baseline
    bool best_key_found = false;
    if (shot_quality>best_quality && strcmp (cipher_key, shot_cipher_key))
    {
      best_quality = shot_quality;
      strcpy (cipher_key, shot_cipher_key);
      hacked_time = last_best_found_time;
      best_key_found = true;
      temp_plain_text = decrypt (cipher_text_clean, clean_text_size, cipher_key);
    }

    // 7. Check if any other process has achieved any better result
    /******************************MPI ANALYZE ALL PROCESS RESULTS***************************/
    if (mpi_rank==MASTER)
    {
      for (int process_id=1; process_id<mpi_total_proc; process_id++)
      {
        MPI_Recv (&mpi_buf_quality, 1, MPI_FLOAT, process_id, 0, MPI_COMM_WORLD, &status);
        MPI_Recv (&mpi_buf_hacked_time, 1, MPI_FLOAT, process_id, 1, MPI_COMM_WORLD, &status);
        MPI_Recv (&mpi_buf_cipher_key[0], 27, MPI_CHAR, process_id, 2, MPI_COMM_WORLD, &status);
        // If the achieved quality ih higher, get the new cipher key
        if (mpi_buf_quality>=best_quality)
        {
          // If the achieved quality is higher and the cipher key is different
          if (strcmp (cipher_key, mpi_buf_cipher_key))
          {
            best_quality = mpi_buf_quality;
            strcpy (cipher_key, mpi_buf_cipher_key);
            hacked_time = getTime(&time1);
          }
          // Else if achieved quality is the same, but the cipher key is the same
          // (!actually this strage behavior does happen, possibly due to rounding error)
          // Then use the better time for reporting purposes
          else if (mpi_buf_hacked_time<hacked_time)
            hacked_time = mpi_buf_hacked_time;
        }
      }

      // 8. Print the result for this shot
      if (best_key_found)
      {
        printf (": <Better Cipher Key Found in %.2fs>\tTime: %.2fs\n\n",hacked_time, getTime(&time1));
        printf ("\tKEY - %s\n\n", cipher_key);
      }
      else
        printf (": <No Better Key>\t\tTime: %.2fs\n", getTime(&time1));
    }
    else
    {
      MPI_Send (&shot_quality, 1, MPI_FLOAT, MASTER, 0, MPI_COMM_WORLD);
      MPI_Send (&hacked_time, 1, MPI_FLOAT, MASTER, 1, MPI_COMM_WORLD);
      MPI_Send (&shot_cipher_key[0], 27, MPI_CHAR, MASTER, 2, MPI_COMM_WORLD);
    }
    /***************************************************************************************/

  }

  // 9. Generate the plain text with the best cipher key
  temp_plain_text = decrypt (cipher_text, text_size, cipher_key);
  strcpy (plain_text, temp_plain_text);

  // 10. Wrapup and return the best cipher key
  delete [] shot_cipher_key;
  delete [] subshot_cipher_key;
  delete [] output_text;
  delete [] inverted_key;
  return cipher_key;
}


/********************************************************************/
/* DECRYPTION: Decrypts the cipher text with the passed cipher key  */
/********************************************************************/
char *decrypt (char *cipher_text, int text_length, char *key)
{
  // 1. Allocate variables

  inverted_key [26] = '\0';

  // 2. Build inverted key
  #pragma novector
  for (int i=0; i<26; i++)
  {
    // Find the letter to map to
    char toChar=0;
    #pragma novector
    for (int j=0; j<26; j++)
      if ((key[j])==i+'A')
      {
        toChar = j+'A';
      }

    // Map it to inverted key
    inverted_key [i] = toChar;
  }

  // 3. Perform monoalphabatic substitution
  #pragma novector
  for (int i=0; i<text_length; i++)
  {
    if (cipher_text[i]>='A' && cipher_text[i]<='Z')
      output_text[i] = inverted_key[cipher_text[i]-'A'];
    else
      output_text[i] = cipher_text[i];
  }
  output_text [text_length] = '\0';

  // 4. Return array address
  return output_text;
}

/***************************************************/
/*        Converts lower case to upper case        */
/***************************************************/
void to_upper (char *input_string)
{
  for (int i=0; i<text_size; i++)
  {
      if (input_string[i]>='a' && input_string[i]<='z')
        input_string[i] -= 32;
  }

}


/*******************************************************************************************************/
/*Assigns a letter to readable text and flags the corresponding map flag to 0-can_be_changed or 1-fixed*/
/*******************************************************************************************************/
void assign_text_value (int index, char value, bool fixed)
{
  readable_text[index]  = value;
  text_map [index]    = fixed;
}


/*******************************************************************************************************/
/*      Uses map flags to determine if a portion of readable text can be treated as a single word      */
/*(If all flags are 0, they can be part of a word, otherwise, that portions consists of multiple words)*/
/*******************************************************************************************************/
bool is_valid_word (int start_ptr, int end_ptr)
{
  for (int i=0; i<end_ptr; i++)
    if (text_map[start_ptr+i]!=0)
      return false;

  return true;
}


/*******************************************************************************************************************/
/*Updates map flags of the portion of text to a same unique number, which increaments as this function gets called */
/*******************************************************************************************************************/
void mark_as_word (int start_ptr, int end_ptr)
{
  static int group_id = 2;

  for (int i=0; i<end_ptr; i++)
    text_map [start_ptr+i]  = group_id;

  group_id ++;
  // group_id = group_id%8 + 2;
}


/*******************************************************************************/
/* CORE: Separate the words in the decoded plain text file into readable format*/
/*******************************************************************************/
void word_separator ()
{
  // 1. Allocate variables
  readable_text = new char [text_size*2];   // Text to store the new readable text
  text_map    = new int [text_size*2];    // Array of size readable_text to assist in making processed text
  text_size   = strlen(plain_text);     // Plain text size

  int text_ptr = 0;               // Index pointer to the readable text

  char prev_letter=0;             // Temp variable to store previous letter
  char cur_letter;                // Temp variable to store current letter
  char next_letter;               // Temp variable to store next letter
  for (int i=0; i<text_size*2; i++)
    text_map [i] = 0;

  // I) Stage 1: Use punctuations and numbers to split into words
  for (int i=0; i<text_size; i++)
  {
    // 1. Assign current and next letter (default previous letter is 0)
    cur_letter  = plain_text[i];
  next_letter = plain_text[i+1];

  // 2. If current letter is a digit
    if (isdigit(cur_letter))
    {
      // a. If previous and next are also digits, then just copy the current number over
      if (isdigit(prev_letter) && isdigit(next_letter))
        assign_text_value (text_ptr++, cur_letter, true);
      // b. If none of the neighboring letters are digits, then add a spzce at either end
      else if (!isdigit(prev_letter) && !isdigit(next_letter))
      {
        if (prev_letter!='$')   // If its a $ sign, dont add space as that would make $1 appear as $ 1
          assign_text_value (text_ptr++, ' ', true);
        assign_text_value (text_ptr++, cur_letter, true);
        if (isalnum(next_letter)) // If there's punctuation sign later, then dont add a space later
          assign_text_value (text_ptr++, ' ', true);
      }
      // c. If only the next letter is a digit, add a space before this digit
      else if (!isdigit(prev_letter))
      {
        if (prev_letter!='$' && prev_letter!='(')   // If its a $ sign, dont add space as that would make $1 appear as $ 1
            assign_text_value (text_ptr++, ' ', true);
        assign_text_value (text_ptr++, cur_letter, true);
      }
      // c. If only the previous letter is a digit, add a space after this digit
      else
      {
        assign_text_value (text_ptr++, cur_letter, true);
        if (isalnum(next_letter)) // If there's punctuation sign later, then dont add a space later
          assign_text_value (text_ptr++, ' ', true);
      }
    }

    // 3. If current letter and next letter forms 's, then add a space after this combination.
    else if (cur_letter =='\'' && next_letter == 'S')
    {
    assign_text_value (text_ptr++, cur_letter, true);
    assign_text_value (text_ptr++, next_letter, true);
    // Add a space only if we're sure that the next character doesn't add another space (to avoid double spacing)
    if (!isdigit(next_letter) && next_letter != '(' && next_letter != '$')
      assign_text_value (text_ptr++, ' ', true);
    i++;
    }

    // 4. If the current letter is one of these punctuations, add a space after it
    else if (cur_letter == '.' || cur_letter == ',' || cur_letter == ':' || cur_letter == ';' || cur_letter == ')' || cur_letter =='%')
    {
      assign_text_value (text_ptr++, cur_letter, true);
      if (!(next_letter == '.' || next_letter == ',' || next_letter == ':' || next_letter == ';' || next_letter == ')' || next_letter =='%'))
        assign_text_value (text_ptr++, ' ', true);
    }

    // 5. If the current letter is one of these punctuations, add a space before it
    else if (cur_letter == '(' || cur_letter == '$')
    {
      assign_text_value (text_ptr++, ' ', true);
      assign_text_value (text_ptr++, cur_letter, true);
    }

    // 6. If the current letter is any other punctuation, dont add any spaces
    else if (!isalnum(cur_letter))
      assign_text_value (text_ptr++, cur_letter, true);

  // 7. If the current letter is actually a letter, then flag it unfixed in text_map
    else
      assign_text_value (text_ptr++, cur_letter, false);

    // 8. Assign current letter to previous letter, to prepare it for next iteration
    prev_letter = cur_letter;
  }

  // II) Stage 2: Identify words using dictionary
  string str_readable_text (readable_text);

  // 1. Try to flag out possible words in the text_map file
  for (int i=0; i<word_list_size; i++)
  {
    size_t pos = 0;
    // 1. Get a word from the dictionary
    pos = str_readable_text.find(dictionary[i], pos);

    // 2. As long as there exists that word in the plain text,
    while (pos !=  string::npos)
  {
    // 3. If the targetted word is a valid candidate (refer to report for details)
    if (is_valid_word (pos, strlen(dictionary[i])))
    {
      // 4. Marks the letters as a word (only mark, they are not yet split)
      mark_as_word (pos, strlen(dictionary[i]));
    }
    // 5. Try to get the next occurence of the word and repeat 2, 3 & 4
    pos = str_readable_text.find(dictionary[i], pos+strlen(dictionary[i]));
  };
  }

  // 6. Perform actual separation of words based on the flag entries in the text_map
  char *temp_text = new char[text_size*2];    // A temporary array to store the current readable_text, as the latter will be modified
  strncpy (temp_text, readable_text, text_ptr);
  for (int i=0, j=0; i<text_ptr; i++)
  {
    // a. Copy over the letter pointed by current index
    readable_text [j++] = temp_text[i];
    // b. Leave a space if it happends that the next letter belongs to another word
    //    If the next letter is one of the letters handled in Stage I, then dont leava a space, as that has been handled in the former section
    if ((text_map[i]!=1)&&(text_map[i+1]!=1) && (text_map[i]!=text_map[i+1]))
      readable_text [j++] = ' ';
  }
}


/***************************************************/
/*       Removes Non-alphabetical Characters       */
/***************************************************/
char *strip_nonalpha (char *input_string)
{
  // 1. Count the resultant string length
  int length = 0;
  #pragma novector
  for (int i=0; i<text_size; i++)
  {
      if ((input_string[i]>='A' && input_string[i]<='Z'))
      {
        length ++;
      }
  }

  // 2. Allocate space for new string
  char *output_string = new char [length+1];

  // 3. Strip unwanted characters
  int ptr=0;
  #pragma novector
  for (int i=0; i<text_size; i++)
  {
      if ((input_string[i]>='A' && input_string[i]<='Z'))
      {
        output_string [ptr++] = input_string [i];
      }
  }
  output_string [ptr] = '\0';

  // 4. Return address of new string
  return output_string;
}

/***************************************************/
/*             Random Number Generator             */
/***************************************************/
int random (int min, int max)
{
  return (min + (rand() % (int)(max - min + 1)));
}

/***************************************************/
/*               Swaps Two Characters              */
/***************************************************/
void swap_char (char *a, char *b)
{
  char temp = *a;
  *a = *b;
  *b = temp;
}


/***************************************************/
/*                Profiling Functions              */
/***************************************************/
void resetTime(clock_t *timer)
{
    // gettimeofday(timer, NULL);
  *timer = clock();
}

float getTime(clock_t *time1)
{
    clock_t time2;
    time2 = clock();
    return (float)(time2-*time1)/CLOCKS_PER_SEC;
}